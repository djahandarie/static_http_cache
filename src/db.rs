use std::cmp;
use std::error;
use std::ffi;
use std::fmt;

use std::path;

use reqwest;
use rusqlite::types::ToSql;
use rusqlite::{Transaction, NO_PARAMS};

type Result<T> = std::result::Result<T, Box<dyn error::Error + Send + Sync + 'static>>;

const SCHEMA_SQL: &str = "
    CREATE TABLE urls (
        url TEXT NOT NULL UNIQUE,
        path TEXT NOT NULL,
        last_modified TEXT,
        etag TEXT
    );
";

/// All the information we have about a given URL.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct CacheRecord {
    /// The path to the cached response body on disk.
    pub path: String,
    /// The value of the Last-Modified header in the original response.
    pub last_modified: Option<String>,
    /// The value of the Etag header in the original response.
    pub etag: Option<String>,
}

fn canonicalize_db_path(path: path::PathBuf) -> Result<path::PathBuf> {
    let mem_path: ffi::OsString = ":memory:".into();

    Ok(if path == mem_path {
        // If it's the special ":memory:" path, use it as-is.
        path.to_path_buf()
    } else {
        let parent = path.parent().unwrap_or(path::Path::new("."));

        // Otherwise, canonicalize it so we can reliably compare instances.
        // The weird joining behaviour is because we require the path
        // to exist, but we don't require the filename to exist.
        parent
            .canonicalize()?
            .join(path.file_name().unwrap_or(ffi::OsStr::new("")))
    })
}

/// Represents the database that describes the contents of the cache.
pub struct CacheDB {
    path: path::PathBuf,
    conn: rusqlite::Connection,
}

impl CacheDB {
    /// Create a cache database in the given file.
    pub fn new(path: path::PathBuf) -> Result<CacheDB> {
        let path = canonicalize_db_path(path)?;
        debug!("Creating cache metadata in {:?}", path);
        let conn = rusqlite::Connection::open(&path)?;

        let nrows: i64 = conn.query_row("SELECT COUNT(*) FROM sqlite_master;", NO_PARAMS, |r| {
            r.get(0)
        })?;
        if nrows == 0 {
            debug!("No tables in the cache DB, loading schema.");
            conn.execute_batch(SCHEMA_SQL)?;
        }

        Ok(CacheDB { path, conn })
    }

    /// Return what the DB knows about a URL, if anything.
    pub fn get(&self, mut url: reqwest::Url) -> Result<CacheRecord> {
        url.set_fragment(None);

        let record = self.conn.query_row(
            "
            SELECT path, last_modified, etag
            FROM urls
            WHERE url = ?
            LIMIT 1
            ",
            &[&url.as_str()],
            |r| {
                Ok(CacheRecord {
                    path: r.get(0)?,
                    last_modified: r.get(1).ok(),
                    etag: r.get(2).ok(),
                })
            },
        );

        record
            .map_err(|_e| From::from(format!("URL not found in cache: {:?}", url)))
            .map(|r| {
                debug!(
                    "Cache says URL {:?} content is at {:?}, etag {:?}, last modified at {:?}",
                    url, r.path, r.etag, r.last_modified
                );
                r
            })
    }

    /// Record information about this information in the database.
    pub fn set(&mut self, mut url: reqwest::Url, record: CacheRecord) -> Result<Transaction> {
        url.set_fragment(None);

        let t = self.conn.transaction()?;

        t.execute(
            "
            INSERT OR REPLACE INTO urls
                (url, path, last_modified, etag)
            VALUES
                (?1, ?2, ?3, ?4);
            ",
            &[
                &url.as_str() as &dyn ToSql,
                &record.path,
                &record.last_modified.map(|date| format!("{}", date)),
                &record.etag.map(|etag| format!("{}", etag)),
            ],
        )?;

        Ok(t)
    }
}

impl fmt::Debug for CacheDB {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "CacheDB {{path: {:?}}}", self.path)
    }
}

impl cmp::PartialEq for CacheDB {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }
}

impl cmp::Eq for CacheDB {}

#[cfg(test)]
mod tests {
    extern crate tempdir;
    use reqwest;
    use rusqlite::NO_PARAMS;

    use std::path;

    #[test]
    fn create_fresh_db() {
        let db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        let name: String = db
            .conn
            .query_row(
                "SELECT name FROM sqlite_master WHERE TYPE = 'table'",
                NO_PARAMS,
                |x| x.get(0),
            )
            .unwrap();

        assert_eq!(name, "urls");
    }

    #[test]
    fn reopen_existing_db() {
        let root = tempdir::TempDir::new("cachedb-test").unwrap().into_path();
        let db_path = root.join("cache.db");

        let db1 = super::CacheDB::new(db_path.clone()).unwrap();
        let name1: String = db1
            .conn
            .query_row(
                "SELECT name FROM sqlite_master WHERE TYPE = 'table'",
                NO_PARAMS,
                |x| x.get(0),
            )
            .unwrap();

        assert_eq!(name1, "urls");

        let db2 = super::CacheDB::new(db_path.clone()).unwrap();
        let name2: String = db2
            .conn
            .query_row(
                "SELECT name FROM sqlite_master WHERE TYPE = 'table'",
                NO_PARAMS,
                |x| x.get(0),
            )
            .unwrap();

        assert_eq!(name2, "urls");
    }

    #[test]
    fn open_bogus_db() {
        let res = super::CacheDB::new(path::PathBuf::new().join("does/not/exist"));

        assert_eq!(res.is_err(), true);
    }

    #[test]
    fn get_from_empty_db() {
        let db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        let err = db.get("http://example.com/".parse().unwrap()).unwrap_err();

        assert_eq!(
            err.description(),
            "URL not found in cache: \"http://example.com/\""
        );
    }

    #[test]
    fn get_unknown_url() {
        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        db.set(
            "http://example.com/one".parse().unwrap(),
            super::CacheRecord {
                path: "path/to/data".into(),
                last_modified: None,
                etag: None,
            },
        )
        .unwrap()
        .commit()
        .unwrap();

        let err = db
            .get("http://example.com/two".parse().unwrap())
            .unwrap_err();

        assert_eq!(
            err.description(),
            "URL not found in cache: \"http://example.com/two\""
        );
    }

    #[test]
    fn get_known_url() {
        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        let orig_record = super::CacheRecord {
            path: "path/to/data".into(),
            last_modified: None,
            etag: None,
        };

        db.set("http://example.com/".parse().unwrap(), orig_record.clone())
            .unwrap()
            .commit()
            .unwrap();

        let new_record = db.get("http://example.com/".parse().unwrap()).unwrap();

        assert_eq!(new_record, orig_record);
    }

    #[test]
    fn get_known_url_with_headers() {
        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        let orig_record = super::CacheRecord {
            path: "path/to/data".into(),
            last_modified: Some("Thu, 01 Jan 1970 00:00:00 GMT".to_owned()),
            etag: Some("some-etag".to_owned()),
        };

        db.set("http://example.com/".parse().unwrap(), orig_record.clone())
            .unwrap()
            .commit()
            .unwrap();

        let new_record = db.get("http://example.com/".parse().unwrap()).unwrap();

        assert_eq!(new_record, orig_record);
    }

    #[test]
    fn get_url_with_invalid_path() {
        let db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        db.conn
            .execute(
                "
                INSERT INTO urls
                    ( url
                    , path
                    , last_modified
                    , etag
                    )
                VALUES
                    ( 'http://example.com/'
                    , CAST('abc' AS BLOB)
                    , NULL
                    , NULL
                    )
                ;
                ",
                NO_PARAMS,
            )
            .unwrap();

        let err = db.get("http://example.com/".parse().unwrap()).unwrap_err();

        assert_eq!(
            err.description(),
            "URL not found in cache: \"http://example.com/\""
        );
    }

    #[test]
    fn get_url_with_invalid_last_modified_and_etag() {
        let db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        db.conn
            .execute(
                "
                INSERT INTO urls
                    ( url
                    , path
                    , last_modified
                    , etag
                    )
                VALUES
                    ( 'http://example.com/'
                    , 'path/to/data'
                    , CAST('abc' AS BLOB)
                    , CAST('def' AS BLOB)
                    )
                ;
                ",
                NO_PARAMS,
            )
            .unwrap();

        let record = db.get("http://example.com/".parse().unwrap()).unwrap();

        assert_eq!(
            record,
            super::CacheRecord {
                path: "path/to/data".into(),
                // We expect TEXT or NULL; if we get a BLOB value we
                // treat it as NULL.
                last_modified: None,
                etag: None,
            }
        );
    }

    #[test]
    fn get_ignores_fragments() {
        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        let orig_record = super::CacheRecord {
            path: "path/to/data".into(),
            last_modified: None,
            etag: None,
        };

        db.set("http://example.com/".parse().unwrap(), orig_record.clone())
            .unwrap()
            .commit()
            .unwrap();

        let new_record = db.get("http://example.com/#top".parse().unwrap()).unwrap();

        assert_eq!(new_record, orig_record);
    }

    #[test]
    fn insert_data_with_commit() {
        let url: reqwest::Url = "http://example.com/".parse().unwrap();
        let record = super::CacheRecord {
            path: "path/to/data".into(),
            last_modified: None,
            etag: None,
        };

        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        // Add data into the DB, inside a block so we can be sure all the
        //  intermediates have been dropped afterward.
        {
            let trans = db.set(url.clone(), record.clone()).unwrap();

            trans.commit().unwrap();
        }

        // Did our data make it into the DB?
        assert_eq!(db.get(url).unwrap(), record);
    }

    #[test]
    fn insert_data_with_all_fields() {
        let url: reqwest::Url = "http://example.com/".parse().unwrap();
        let record = super::CacheRecord {
            path: "path/to/data".into(),
            last_modified: Some("Thu, 01 Jan 1970 00:00:00 GMT".to_owned()),
            etag: Some("some-etag".to_owned()),
        };

        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        // Add data into the DB, inside a block so we can be sure all the
        //  intermediates have been dropped afterward.
        db.set(url.clone(), record.clone())
            .unwrap()
            .commit()
            .unwrap();

        // Did our data make it into the DB?
        assert_eq!(db.get(url).unwrap(), record);
    }

    #[test]
    fn insert_data_without_commit() {
        let url: reqwest::Url = "http://example.com/".parse().unwrap();
        let record = super::CacheRecord {
            path: "path/to/data".into(),
            last_modified: None,
            etag: None,
        };

        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        // Add data into the DB, inside a block so we can be sure all the
        //  intermediates have been dropped afterward.
        {
            let _ = db.set(url.clone(), record.clone()).unwrap();

            // Don't commit before the end of the block!
        }

        // Did our data make it into the DB?
        assert_eq!(
            db.get(url).unwrap_err().description(),
            "URL not found in cache: \"http://example.com/\""
        );
    }

    #[test]
    fn overwrite_data() {
        let url: reqwest::Url = "http://example.com/".parse().unwrap();

        let record_one = super::CacheRecord {
            path: "path/to/data/one".into(),
            last_modified: None,
            etag: Some("one".to_owned()),
        };

        let record_two = super::CacheRecord {
            path: "path/to/data/two".into(),
            last_modified: None,
            etag: Some("two".to_owned()),
        };

        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        // Our example URL just returned record one.
        db.set(url.clone(), record_one.clone())
            .unwrap()
            .commit()
            .unwrap();

        // We recorded that correctly, right?
        assert_eq!(db.get(url.clone()).unwrap(), record_one);

        // Oh, the URL got updated!
        db.set(url.clone(), record_two.clone())
            .unwrap()
            .commit()
            .unwrap();

        // We recorded that correctly too, right?
        assert_eq!(db.get(url.clone()).unwrap(), record_two);
    }

    #[test]
    fn insert_data_ignores_url_fragment() {
        let record_one = super::CacheRecord {
            path: "path/to/data/one".into(),
            last_modified: None,
            etag: Some("one".to_owned()),
        };

        let record_two = super::CacheRecord {
            path: "path/to/data/two".into(),
            last_modified: None,
            etag: Some("two".to_owned()),
        };

        let mut db = super::CacheDB::new(path::PathBuf::new().join(":memory:")).unwrap();

        // Try to insert data with a fragment
        db.set(
            "http://example.com/#frag".parse().unwrap(),
            record_one.clone(),
        )
        .unwrap()
        .commit()
        .unwrap();

        // Try to insert different data without a fragment
        db.set("http://example.com/".parse().unwrap(), record_two.clone())
            .unwrap()
            .commit()
            .unwrap();

        // Querying with any fragment, or without a fragment, will always
        // give us the same information.
        assert_eq!(
            db.get("http://example.com/#frag".parse().unwrap()).unwrap(),
            record_two
        );
        assert_eq!(
            db.get("http://example.com/#garf".parse().unwrap()).unwrap(),
            record_two
        );
        assert_eq!(
            db.get("http://example.com/".parse().unwrap()).unwrap(),
            record_two
        );

        // If we insert data with a fragment, the new data is returned for
        // all queries.
        db.set(
            "http://example.com/#boop".parse().unwrap(),
            record_one.clone(),
        )
        .unwrap()
        .commit()
        .unwrap();

        assert_eq!(
            db.get("http://example.com/#frag".parse().unwrap()).unwrap(),
            record_one
        );
        assert_eq!(
            db.get("http://example.com/#garf".parse().unwrap()).unwrap(),
            record_one
        );
        assert_eq!(
            db.get("http://example.com/".parse().unwrap()).unwrap(),
            record_one
        );
    }

    #[test]
    fn dbs_are_equal_if_paths_are_equal() {
        let root = tempdir::TempDir::new("cachedb-test").unwrap().into_path();
        let db_path = root.join("cache.db");

        let db1 = super::CacheDB::new(db_path.clone()).unwrap();
        let db2 = super::CacheDB::new(db_path.clone()).unwrap();

        assert_eq!(db1, db2);
    }

    #[test]
    fn dbs_not_equal_if_paths_are_different() {
        let root = tempdir::TempDir::new("cachedb-test").unwrap().into_path();

        let db1 = super::CacheDB::new(root.join("cache-1.db")).unwrap();
        let db2 = super::CacheDB::new(root.join("cache-2.db")).unwrap();

        assert_ne!(db1, db2);
    }

    #[test]
    fn db_debug_prints_path() {
        let root = tempdir::TempDir::new("cachedb-test").unwrap().into_path();
        let path = root.join("cache.db");

        let db = super::CacheDB::new(path.clone()).unwrap();

        assert_eq!(
            format!("my db: {:?}", db),
            format!("my db: CacheDB {{path: {:?}}}", path)
        );
    }
}
