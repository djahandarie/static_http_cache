//! Traits describing parts of the `reqwest` library, so that we can override
//! them in tests.
//!
//! You do not need to care about this module
//! if you just want to use this crate.
use bytes::Bytes;
use reqwest;
use std::error::Error;
use std::fmt;

/// Represents the result of sending an HTTP request.
///
/// Modelled after `reqwest::Response`.
#[async_trait]
pub trait HttpResponse: fmt::Debug
where
    Self: ::std::marker::Sized,
{
    /// Obtain access to the headers of the response.
    fn headers(&self) -> &reqwest::header::HeaderMap;

    /// Obtain a copy of the response's status.
    fn status(&self) -> reqwest::StatusCode;

    /// Return an error if the response's status is in the range 400-599.
    fn error_for_status(self) -> Result<Self, Box<dyn Error>>;

    async fn bytes(self) -> Result<Bytes, Box<dyn Error>>;
}

#[async_trait]
impl HttpResponse for reqwest::Response {
    fn headers(&self) -> &reqwest::header::HeaderMap {
        self.headers()
    }
    fn status(&self) -> reqwest::StatusCode {
        self.status()
    }
    fn error_for_status(self) -> Result<Self, Box<dyn Error>> {
        Ok(self.error_for_status()?)
    }
    async fn bytes(self) -> Result<Bytes, Box<dyn Error>> {
        Ok(self.bytes().await?)
    }
}

/// Represents a thing that can send requests.
///
/// Modelled after `reqwest::Client`.
#[async_trait]
pub trait Client: Send + Sync {
    /// Sending a request produces this kind of response.
    type Response: HttpResponse;

    /// Send the given request and return the response (or an error).
    async fn execute(&self, request: reqwest::Request) -> Result<Self::Response, Box<dyn Error>>;
}

#[async_trait]
impl Client for reqwest::Client {
    type Response = reqwest::Response;

    async fn execute(&self, request: reqwest::Request) -> Result<Self::Response, Box<dyn Error>> {
        Ok(self.execute(request).await?)
    }
}

#[cfg(test)]
pub mod tests {
    use reqwest;

    use bytes::Bytes;
    use core::sync::atomic::{AtomicBool, Ordering};

    use std::fmt;
    use std::io;

    use std::error::Error;
    use std::io::Read;

    #[derive(Debug, Eq, PartialEq, Hash)]
    pub struct FakeError;

    impl fmt::Display for FakeError {
        fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
            f.write_str("FakeError")?;
            Ok(())
        }
    }

    impl Error for FakeError {
        fn description(&self) -> &str {
            "Something Ooo occurred"
        }
        fn cause(&self) -> Option<&dyn Error> {
            None
        }
    }

    #[derive(Clone, Debug)]
    pub struct FakeResponse {
        pub status: reqwest::StatusCode,
        pub headers: reqwest::header::HeaderMap,
        pub body: io::Cursor<Vec<u8>>,
    }

    #[async_trait]
    impl super::HttpResponse for FakeResponse {
        fn headers(&self) -> &reqwest::header::HeaderMap {
            &self.headers
        }
        fn status(&self) -> reqwest::StatusCode {
            self.status
        }
        fn error_for_status(self) -> Result<Self, Box<dyn Error>> {
            if !self.status.is_client_error() && !self.status.is_server_error() {
                Ok(self)
            } else {
                Err(Box::new(FakeError))
            }
        }
        async fn bytes(self) -> Result<Bytes, Box<dyn Error>> {
            Ok(Bytes::copy_from_slice(&*self.body.get_ref()))
        }
    }

    impl Read for FakeResponse {
        fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
            self.body.read(buf)
        }
    }

    pub struct FakeClient {
        pub expected_url: reqwest::Url,
        pub expected_headers: reqwest::header::HeaderMap,
        pub response: FakeResponse,
        called: AtomicBool,
    }

    impl FakeClient {
        pub fn new(
            expected_url: reqwest::Url,
            expected_headers: reqwest::header::HeaderMap,
            response: FakeResponse,
        ) -> FakeClient {
            let called = AtomicBool::new(false);
            FakeClient {
                expected_url,
                expected_headers,
                response,
                called,
            }
        }

        pub fn assert_called(self) {
            assert_eq!(self.called.load(Ordering::Relaxed), true);
        }
    }

    #[async_trait]
    impl super::Client for FakeClient {
        type Response = FakeResponse;

        async fn execute(
            &self,
            request: reqwest::Request,
        ) -> Result<Self::Response, Box<dyn Error>> {
            assert_eq!(request.method(), &reqwest::Method::GET);
            assert_eq!(request.url(), &self.expected_url);
            assert_eq!(request.headers(), &self.expected_headers);

            self.called.store(true, Ordering::Relaxed);

            Ok(self.response.clone())
        }
    }

    pub struct BrokenClient<F>
    where
        F: Fn() -> Box<dyn Error>,
        F: Sync,
    {
        pub expected_url: reqwest::Url,
        pub expected_headers: reqwest::header::HeaderMap,
        pub make_error: F,
        called: AtomicBool,
    }

    impl<F> BrokenClient<F>
    where
        F: Fn() -> Box<dyn Error>,
        F: Sync,
    {
        pub fn new(
            expected_url: reqwest::Url,
            expected_headers: reqwest::header::HeaderMap,
            make_error: F,
        ) -> BrokenClient<F> {
            let called = AtomicBool::new(false);
            BrokenClient {
                expected_url,
                expected_headers,
                make_error,
                called,
            }
        }

        pub fn assert_called(self) {
            assert_eq!(self.called.load(Ordering::Relaxed), true);
        }
    }

    #[async_trait]
    impl<F> super::Client for BrokenClient<F>
    where
        F: Fn() -> Box<dyn Error>,
        F: Send + Sync,
    {
        type Response = FakeResponse;

        async fn execute(
            &self,
            request: reqwest::Request,
        ) -> Result<Self::Response, Box<dyn Error>> {
            assert_eq!(request.method(), &reqwest::Method::GET);
            assert_eq!(request.url(), &self.expected_url);
            assert_eq!(request.headers(), &self.expected_headers);

            self.called.store(true, Ordering::Relaxed);

            Err((self.make_error)())
        }
    }
}
