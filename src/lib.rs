#![doc(html_root_url = "https://docs.rs/static_http_cache/0.1.0")]
//! Introduction
//! ============
//!
//! `static_http_cache` is a local cache for static HTTP resources.
//!
//! This library maintains a cache of HTTP resources
//! in a local directory you specify.
//! Whenever you ask it for the contents of a URL,
//! it will re-use a previously-downloaded copy
//! if the resource has not changed on the server.
//! Otherwise,
//! it will download the new version and use that instead.
//!
//! Because it only supports static resources,
//! `static_http_cache` only sends HTTP `GET` requests.
//!
//! `static_http_cache` uses the [Reqwest][rq] crate for HTTP operations,
//! so it should properly handle HTTPS negotiation
//! and use the operating-system's certificate store.
//!
//! Currently,
//! `static_http_cache` only uses the `Last-Modified` and `ETag` HTTP headers
//! to determine when its cached data is out of date.
//! Therefore,
//! it's not suitable for general-purpose HTTP caching;
//! it's best suited for static content like Amazon S3 data,
//! or Apache or nginx serving up a filesystem directory.
//!
//! [rq]: https://crates.io/crates/reqwest
//!
//! First Example
//! =============
//!
//! To use this crate, you need to construct a [`Cache`]
//! then call its [`get`] method:
//!
//!     extern crate reqwest;
//!     extern crate static_http_cache;
//!
//!     use std::error::Error;
//!     use std::fs::File;
//!     use std::path::PathBuf;
//!
//!     async fn get_my_resource() -> Result<File, Box<dyn Error>> {
//!         let mut cache = static_http_cache::Cache::new(
//!             PathBuf::from("my_cache_directory"),
//!             reqwest::Client::new(),
//!         )?;
//!
//!         cache.get(reqwest::Url::parse("http://example.com/some-resource")?).await
//!     }
//!
//! For repeated queries in the same program,
//! you'd probably want to create the `Cache` once
//! and call `get` repeatedly,
//! of course.
//!
//! [`Cache`]: struct.Cache.html
//! [`get`]: struct.Cache.html#method.get
//!
//! For a complete, minimal example of how to use `static_http_cache`,
//! see the included [simple example][ex].
//!
//! [ex]: https://gitlab.com/Screwtapello/static_http_cache/blob/master/examples/simple.rs
//!
//! Capabilities
//! ============
//!
//! Alternative HTTP backends
//! -------------------------
//!
//! Although `static_http_cache` is designed to work with the `reqwest` library,
//! it will accept any type that implements
//! the traits in the [`reqwest_mock`] module.
//! If you want to use it with an alternative HTTP backend,
//! or if you need to stub out network access for testing purposes,
//! you can do that.
//!
//! [`reqwest_mock`]: reqwest_mock/index.html
//!
//! Concurrent cache sharing
//! ------------------------
//!
//! Cache metadata is stored in a SQLite database,
//! so it's safe to give different threads
//! (or even different processes)
//! their own [`Cache`] instance
//! backed by the same filesystem path.
//!
//! Note that while it's *safe* to have multiple things
//! managing the same cache,
//! it's not necessarily performant:
//! a [`Cache`] instance that's downloading a new or updated file
//! is likely to stall other cache reads or writes
//! until it's complete.

#[macro_use]
extern crate async_trait;
extern crate crypto_hash;
#[macro_use]
extern crate log;
extern crate chrono;
extern crate rand;
extern crate reqwest;
extern crate rusqlite;

extern crate futures;

#[cfg(test)]
#[macro_use]
extern crate futures_await_test;

use futures::lock::Mutex;
use std::error;
use std::fs;
use std::io;
use std::io::Write;
use std::iter;
use std::path;

use reqwest::header as rh;

pub mod reqwest_mock;

mod db;

type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

fn make_random_file<P: AsRef<path::Path>>(parent: P) -> Result<(fs::File, path::PathBuf)> {
    use rand::distributions::Alphanumeric;
    use rand::Rng;
    let mut rng = rand::thread_rng();

    loop {
        let new_path = parent.as_ref().join(
            iter::repeat(())
                .map(|()| rng.sample(Alphanumeric))
                .take(20)
                .collect::<String>(),
        );

        match fs::OpenOptions::new()
            .create_new(true)
            .write(true)
            .open(&new_path)
        {
            Ok(handle) => return Ok((handle, new_path)),
            Err(e) => {
                if e.kind() != io::ErrorKind::AlreadyExists {
                    // An actual error, we'd better report it!
                    return Err(e.into());
                }

                // Otherwise, we just picked a bad name. Let's go back
                // around the loop and try again.
            }
        };
    }
}

/// Represents a local cache of HTTP resources.
///
/// Whenever you ask it for the contents of a URL,
/// it will re-use a previously-downloaded copy
/// if the resource has not changed on the server.
/// Otherwise,
/// it will download the new version and use that instead.
///
/// See [an example](index.html#first-example).
///
/// [`reqwest_mock::Client`]: reqwest_mock/trait.Client.html
/// [`Cache`]: struct.Cache.html
#[derive(Debug)]
pub struct Cache<C: reqwest_mock::Client> {
    root: path::PathBuf,
    db: Mutex<db::CacheDB>,
    client: C,
}

impl<C: reqwest_mock::Client> Cache<C> {
    /// Returns a Cache that wraps `client` and caches data in `root`.
    ///
    /// If the directory `root` does not exist, it will be created.
    /// If multiple instances share the same `root`
    /// (concurrently or in series),
    /// each instance will be able to re-use resources downloaded by
    /// the others.
    ///
    /// For best results,
    /// choose a `root` that is directly attached to
    /// the computer running your program,
    /// such as somewhere inside the `%LOCALAPPDATA%` directory on Windows,
    /// or the `$XDG_CACHE_HOME` directory on POSIX systems.
    ///
    /// `client` should almost certainly be a `reqwest::Client`,
    /// but you can use any type that implements [`reqwest_mock::Client`]
    /// if you want to use a different HTTP client library
    /// or a test double of some kind.
    ///
    ///     # extern crate reqwest;
    ///     # extern crate static_http_cache;
    ///     # use std::error::Error;
    ///     # use std::fs::File;
    ///     # use std::path::PathBuf;
    ///     # fn get_my_resource() -> Result<(), Box<dyn Error>> {
    ///     let mut cache = static_http_cache::Cache::new(
    ///         PathBuf::from("my_cache_directory"),
    ///         reqwest::Client::new(),
    ///     )?;
    ///     # Ok(())
    ///     # }
    ///
    /// [`reqwest_mock::Client`]: reqwest_mock/trait.Client.html
    ///
    /// Errors
    /// ======
    ///
    /// This method may return an error:
    ///
    ///   - if `root` cannot be created, or cannot be written to
    ///   - if the metadata database cannot be created or cannot be written to
    ///   - if the metadata database is corrupt
    ///
    /// In all cases, it should be safe to blow away the entire directory
    /// and start from scratch.
    /// It's only cached data, after all.
    pub fn new(root: path::PathBuf, client: C) -> Result<Cache<C>> {
        fs::DirBuilder::new().recursive(true).create(&root)?;

        let db = Mutex::new(
            db::CacheDB::new(root.join("cache.db")).map_err(|e| e as Box<dyn error::Error>)?,
        );

        Ok(Cache { root, db, client })
    }

    /// Retrieve the content of the given URL.
    ///
    /// If we've never seen this URL before,
    /// we will try to retrieve it
    /// (with a `GET` request)
    /// and store its data locally.
    ///
    /// If we have seen this URL before, we will ask the server
    /// whether our cached data is stale.
    /// If our data is stale,
    /// we'll download the new version
    /// and store it locally.
    /// If our data is fresh,
    /// we'll re-use the local copy we already have.
    ///
    /// If we can't talk to the server to see if our cached data is stale,
    /// we'll silently re-use the data we have.
    ///
    /// Returns a file-handle to the local copy of the data, open for
    /// reading.
    ///
    ///     # extern crate reqwest;
    ///     # extern crate static_http_cache;
    ///     # use std::error::Error;
    ///     # use std::fs::File;
    ///     # use std::path::PathBuf;
    ///     # async fn get_my_resource() -> Result<(), Box<dyn Error>> {
    ///     # let mut cache = static_http_cache::Cache::new(
    ///     #     PathBuf::from("my_cache_directory"),
    ///     #     reqwest::Client::new(),
    ///     # )?;
    ///     let file = cache.get(reqwest::Url::parse("http://example.com/some-resource")?).await?;
    ///     # Ok(())
    ///     # }
    ///
    /// Errors
    /// ======
    ///
    /// This method may return an error:
    ///
    ///   - if the cache metadata is corrupt
    ///   - if the requested resource is not cached,
    ///     and we can't connect to/download it
    ///   - if we can't update the cache metadata
    ///   - if the cache metadata points to a local file that no longer exists
    ///
    /// After returning a network-related or disk I/O-related error,
    /// this `Cache` instance should be OK and you may keep using it.
    /// If it returns a database-related error,
    /// the on-disk storage *should* be OK,
    /// so you might want to destroy this `Cache` instance
    /// and create a new one pointing at the same location.
    pub async fn get(&self, mut url: reqwest::Url) -> Result<fs::File> {
        use reqwest::StatusCode;
        use reqwest_mock::HttpResponse;

        url.set_fragment(None);

        let x = self.db.lock().await.get(url.clone());

        let response = match x {
            Ok(db::CacheRecord {
                path: p,
                last_modified: lm,
                etag: et,
            }) => {
                // We have a locally-cached copy, let's check whether the
                // copy on the server has changed.
                let mut request = reqwest::Request::new(reqwest::Method::GET, url.clone());
                if let Some(timestamp) = lm {
                    request
                        .headers_mut()
                        .insert(rh::IF_MODIFIED_SINCE, timestamp.parse().unwrap());
                }
                if let Some(etag) = et {
                    request
                        .headers_mut()
                        .insert(rh::IF_NONE_MATCH, etag.parse().unwrap());
                }

                info!("Sending HTTP request: {:?}", request);

                let maybe_validation = self
                    .client
                    .execute(request)
                    .await
                    .and_then(|resp| resp.error_for_status());

                match maybe_validation {
                    Ok(new_response) => {
                        info!("Got HTTP response: {:?}", new_response);

                        // If our existing cached data is still fresh...
                        if new_response.status() == StatusCode::NOT_MODIFIED {
                            // ... let's use it as is.
                            return Ok(fs::File::open(self.root.join(p))?);
                        }

                        // Otherwise, we got a new response we need to cache.
                        new_response
                    }
                    Err(e) => {
                        warn!("Could not validate cached response: {}", e);

                        // Let's just use the existing data we have.
                        return Ok(fs::File::open(self.root.join(p))?);
                    }
                }
            }
            Err(_) => {
                // This URL isn't in the cache, or we otherwise can't find it.
                self.client
                    .execute(reqwest::Request::new(reqwest::Method::GET, url.clone()))
                    .await?
                    .error_for_status()?
            }
        };

        let content_dir = self.root.join("content");
        fs::DirBuilder::new().recursive(true).create(&content_dir)?;
        let (mut handle, path) = make_random_file(&content_dir)?;

        let rel_path = path.strip_prefix(&self.root)?;
        let cache_record = db::CacheRecord {
            // We can be sure the relative path is valid UTF-8,
            // because make_random_file() just generated it from ASCII.
            path: rel_path.to_str().unwrap().into(),
            last_modified: response
                .headers()
                .get(rh::LAST_MODIFIED)
                .and_then(|h| h.to_str().ok().map(|s| s.to_owned())),
            etag: response
                .headers()
                .get(rh::ETAG)
                .and_then(|h| h.to_str().ok().map(|s| s.to_owned())),
        };
        let count = handle.write(&response.bytes().await?)?;

        debug!("Downloaded {} bytes", count);

        self.db
            .lock()
            .await
            .set(url, cache_record)
            .map_err(|e| e as Box<dyn error::Error>)?
            .commit()?;

        Ok(fs::File::open(&path)?)
    }
}

#[cfg(test)]
mod tests {
    extern crate env_logger;
    extern crate tempdir;

    use reqwest;
    use reqwest::header as rh;

    use std::io;

    use std::io::Read;

    use super::reqwest_mock::tests as rmt;

    fn make_test_cache(client: rmt::FakeClient) -> super::Cache<rmt::FakeClient> {
        super::Cache::new(
            tempdir::TempDir::new("http-cache-test")
                .unwrap()
                .into_path(),
            client,
        )
        .unwrap()
    }

    fn is_send<T: Send>(_: T) {}

    #[test]
    fn test_send() {
        let _ = env_logger::try_init();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();

        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            rh::HeaderMap::default(),
            rmt::FakeResponse {
                status: reqwest::StatusCode::INTERNAL_SERVER_ERROR,
                headers: rh::HeaderMap::default(),
                body: io::Cursor::new(vec![]),
            },
        ));
        is_send(c.get(url));
    }

    #[async_test]
    async fn initial_request_success() {
        let _ = env_logger::try_init();

        let url_text = "http://example.com/";
        let url: reqwest::Url = url_text.parse().unwrap();

        let body = b"hello world";

        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            rh::HeaderMap::default(),
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: rh::HeaderMap::default(),
                body: io::Cursor::new(body.as_ref().into()),
            },
        ));

        // We should get a file-handle containing the body bytes.
        let mut res = c.get(url).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, body);
        c.client.assert_called();
    }

    #[async_test]
    async fn initial_request_failure() {
        let _ = env_logger::try_init();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();
        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            rh::HeaderMap::default(),
            rmt::FakeResponse {
                status: reqwest::StatusCode::INTERNAL_SERVER_ERROR,
                headers: rh::HeaderMap::default(),
                body: io::Cursor::new(vec![]),
            },
        ));

        let err = c.get(url).await.expect_err("Got a response??");
        assert_eq!(format!("{}", err), "FakeError");
        c.client.assert_called();
    }

    #[async_test]
    async fn ignore_fragment_in_url() {
        let _ = env_logger::try_init();

        let url_fragment: reqwest::Url = "http://example.com/#frag".parse().unwrap();

        let mut network_url = url_fragment.clone();
        network_url.set_fragment(None);

        let mut c = make_test_cache(rmt::FakeClient::new(
            // We expect the cache to request the URL without the fragment.
            network_url,
            rh::HeaderMap::default(),
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: rh::HeaderMap::default(),
                body: io::Cursor::new(b"hello world"[..].into()),
            },
        ));

        // Ask for the URL with the fragment.
        c.get(url_fragment).await.unwrap();
    }

    #[async_test]
    async fn use_cache_data_if_not_modified_since() {
        let _ = env_logger::try_init();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();
        let body = b"hello world";

        let now: String = format!("{}", chrono::Utc::now().format("%a, %e %b %Y %T GMT"));

        // We send a request, and the server responds with the data,
        // and a "Last-Modified" header.
        let mut response_headers = rh::HeaderMap::default();
        response_headers.insert(rh::LAST_MODIFIED, now.parse().unwrap());

        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            rh::HeaderMap::default(),
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: response_headers.clone(),
                body: io::Cursor::new(body.as_ref().into()),
            },
        ));

        // The response and its last-modified date should now be recorded
        // in the cache.
        c.get(url.clone()).await.unwrap();
        c.client.assert_called();

        // For the next request, we expect the request to include the
        // modified date in the "if modified since" header, and we'll give
        // the "no, it hasn't been modified" response.
        let mut second_request = rh::HeaderMap::default();
        second_request.insert(rh::IF_MODIFIED_SINCE, now.parse().unwrap());

        c.client = rmt::FakeClient::new(
            url.clone(),
            second_request,
            rmt::FakeResponse {
                status: reqwest::StatusCode::NOT_MODIFIED,
                headers: response_headers,
                body: io::Cursor::new(b""[..].into()),
            },
        );

        // Now when we make the request, even though the actual response
        // did not include a body, we should get the complete body from
        // the local cache.
        let mut res = c.get(url).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, body);
        c.client.assert_called();
    }

    #[async_test]
    async fn update_cache_if_modified_since() {
        let _ = env_logger::try_init();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();

        // We send a request, and the server responds with the data,
        // and a "Last-Modified" header.
        let request_1_headers = rh::HeaderMap::default();
        let mut response_1_headers = rh::HeaderMap::default();
        response_1_headers.insert(
            rh::LAST_MODIFIED,
            "Thu, 01 Jan 1970 00:00:00 GMT".parse().unwrap(),
        );

        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            request_1_headers,
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: response_1_headers,
                body: io::Cursor::new(b"hello".as_ref().into()),
            },
        ));

        // The response and its last-modified date should now be recorded
        // in the cache.
        c.get(url.clone()).await.unwrap();
        c.client.assert_called();

        // For the next request, we expect the request to include the
        // modified date in the "if modified since" header, and we'll give
        // the "yes, it has been modified" response with a new Last-Modified.
        let mut request_2_headers = rh::HeaderMap::default();
        request_2_headers.insert(
            rh::IF_MODIFIED_SINCE,
            "Thu, 01 Jan 1970 00:00:00 GMT".parse().unwrap(),
        );
        let mut response_2_headers = rh::HeaderMap::default();
        response_2_headers.insert(
            rh::LAST_MODIFIED,
            "Thu, 01 Jan 1970 00:01:00 GMT".parse().unwrap(),
        );

        c.client = rmt::FakeClient::new(
            url.clone(),
            request_2_headers,
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: response_2_headers,
                body: io::Cursor::new(b"world".as_ref().into()),
            },
        );

        // Now when we make the request, we should get the new body and
        // ignore what's in the cache.
        let mut res = c.get(url.clone()).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, b"world");
        c.client.assert_called();

        // If we make another request, we should set If-Modified-Since
        // to match the second response, and be able to return the data from
        // the second response.
        let mut request_3_headers = rh::HeaderMap::default();
        request_3_headers.insert(
            rh::IF_MODIFIED_SINCE,
            "Thu, 01 Jan 1970 00:01:00 GMT".parse().unwrap(),
        );
        let response_3_headers = rh::HeaderMap::default();

        c.client = rmt::FakeClient::new(
            url.clone(),
            request_3_headers,
            rmt::FakeResponse {
                status: reqwest::StatusCode::NOT_MODIFIED,
                headers: response_3_headers,
                body: io::Cursor::new(b"".as_ref().into()),
            },
        );

        // Now when we make the request, we should get updated info from the
        // cache.
        let mut res = c.get(url).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, b"world");
        c.client.assert_called();
    }

    #[async_test]
    async fn return_existing_data_on_connection_refused() {
        let _ = env_logger::try_init();

        let temp_path = tempdir::TempDir::new("http-cache-test")
            .unwrap()
            .into_path();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();

        // We send a request, and the server responds with the data,
        // and a "Last-Modified" header.
        let request_1_headers = rh::HeaderMap::default();
        let mut response_1_headers = rh::HeaderMap::default();
        response_1_headers.insert(
            rh::LAST_MODIFIED,
            "Thu, 01 Jan 1970 00:00:00 GMT".parse().unwrap(),
        );

        let mut c = super::Cache::new(
            temp_path.clone(),
            rmt::FakeClient::new(
                url.clone(),
                request_1_headers,
                rmt::FakeResponse {
                    status: reqwest::StatusCode::OK,
                    headers: response_1_headers,
                    body: io::Cursor::new(b"hello".as_ref().into()),
                },
            ),
        )
        .unwrap();

        // The response and its last-modified date should now be recorded
        // in the cache.
        c.get(url.clone()).await.unwrap();
        c.client.assert_called();

        // If we make second request, we should set If-Modified-Since
        // to match the first response's Last-Modified.
        let mut request_2_headers = rh::HeaderMap::default();
        request_2_headers.insert(
            rh::IF_MODIFIED_SINCE,
            "Thu, 01 Jan 1970 00:00:00 GMT".parse().unwrap(),
        );

        // This time, however, the request will return an error.
        let mut c = super::Cache::new(
            temp_path.clone(),
            rmt::BrokenClient::new(url.clone(), request_2_headers, || rmt::FakeError.into()),
        )
        .unwrap();

        // Now when we request a URL, we should get the cached result.
        let mut res = c.get(url.clone()).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, b"hello");
        c.client.assert_called();
    }

    #[async_test]
    async fn use_cache_data_if_some_match() {
        let _ = env_logger::try_init();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();
        let body = b"hello world";

        // We send a request, and the server responds with the data,
        // and an "Etag" header.
        let mut response_headers = rh::HeaderMap::default();
        response_headers.insert(rh::ETAG, "abcd".parse().unwrap());

        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            rh::HeaderMap::default(),
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: response_headers.clone(),
                body: io::Cursor::new(body.as_ref().into()),
            },
        ));

        // The response and its etag should now be recorded
        // in the cache.
        c.get(url.clone()).await.unwrap();
        c.client.assert_called();

        // For the next request, we expect the request to include the
        // etag in the "if none match" header, and we'll give
        // the "no, it hasn't been modified" response.
        let mut second_request = rh::HeaderMap::default();
        second_request.insert(rh::IF_NONE_MATCH, "abcd".parse().unwrap());

        c.client = rmt::FakeClient::new(
            url.clone(),
            second_request,
            rmt::FakeResponse {
                status: reqwest::StatusCode::NOT_MODIFIED,
                headers: response_headers,
                body: io::Cursor::new(b""[..].into()),
            },
        );

        // Now when we make the request, even though the actual response
        // did not include a body, we should get the complete body from
        // the local cache.
        let mut res = c.get(url).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, body);
        c.client.assert_called();
    }

    #[async_test]
    async fn update_cache_if_none_match() {
        let _ = env_logger::try_init();

        let url: reqwest::Url = "http://example.com/".parse().unwrap();

        // We send a request, and the server responds with the data,
        // and an "ETag" header.
        let request_1_headers = rh::HeaderMap::default();
        let mut response_1_headers = rh::HeaderMap::default();
        response_1_headers.insert(rh::ETAG, "abcd".parse().unwrap());

        let mut c = make_test_cache(rmt::FakeClient::new(
            url.clone(),
            request_1_headers,
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: response_1_headers,
                body: io::Cursor::new(b"hello".as_ref().into()),
            },
        ));

        // The response and its etag should now be recorded in the cache.
        c.get(url.clone()).await.unwrap();
        c.client.assert_called();

        // For the next request, we expect the request to include the
        // etag in the "if none match" header, and we'll give
        // the "yes, it has been modified" response with a new etag.
        let mut request_2_headers = rh::HeaderMap::default();
        request_2_headers.insert(rh::IF_NONE_MATCH, "abcd".parse().unwrap());
        let mut response_2_headers = rh::HeaderMap::default();
        response_2_headers.insert(rh::ETAG, "efgh".parse().unwrap());

        c.client = rmt::FakeClient::new(
            url.clone(),
            request_2_headers,
            rmt::FakeResponse {
                status: reqwest::StatusCode::OK,
                headers: response_2_headers,
                body: io::Cursor::new(b"world".as_ref().into()),
            },
        );

        // Now when we make the request, we should get the new body and
        // ignore what's in the cache.
        let mut res = c.get(url.clone()).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, b"world");
        c.client.assert_called();

        // If we make another request, we should set If-None-Match
        // to match the second response, and be able to return the data from
        // the second response.
        let mut request_3_headers = rh::HeaderMap::default();
        request_3_headers.insert(rh::IF_NONE_MATCH, "efgh".parse().unwrap());
        let response_3_headers = rh::HeaderMap::default();

        c.client = rmt::FakeClient::new(
            url.clone(),
            request_3_headers,
            rmt::FakeResponse {
                status: reqwest::StatusCode::NOT_MODIFIED,
                headers: response_3_headers,
                body: io::Cursor::new(b"".as_ref().into()),
            },
        );

        // Now when we make the request, we should get updated info from the
        // cache.
        let mut res = c.get(url).await.unwrap();
        let mut buf = vec![];
        res.read_to_end(&mut buf).unwrap();
        assert_eq!(&buf, b"world");
        c.client.assert_called();
    }

    // See also: https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching
}
